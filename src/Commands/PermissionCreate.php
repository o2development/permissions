<?php
namespace O2Development\Permissions\Commands;

use Illuminate\Console\Command;
use O2Development\Permissions\Contracts\Permission as Contract;
use Spatie\Permission\Contracts\Permission as PermissionContract;

class PermissionCreate extends Command
{
    protected $signature = 'permission:create 
                {name : The name of the permission} 
                {guard? : The name of the guard}';
    protected $description = 'Create a permission';
    public function handle()
    {
        $permissionClass = app(Contract::class);
        $permission = $permissionClass::create([
            'name' => $this->argument('name'),
            'guard_name' => $this->argument('guard'),
        ]);
        $this->info("Permission `{$permission->name}` created");
    }
}