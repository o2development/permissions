<?php
return [

    'entity_table' => 'first',
    'meta_table' => 'test1',
    'entities' => [
        'entity_table' => 'barf',
        'meta_table' => 'test2'
    ],
    /*
     * Taxonomies
     */
    'taxonomies' => [
        /*
         * Terms table
         */
        'table_terms' => 'terms',

        /*
         * Taxonomies table
         */
        'table_taxonomies' => 'taxonomies',

        /*
         * Relationship table
         */
        'table_pivot' => 'taxables',
    ],
];