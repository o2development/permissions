<?php

namespace O2Development\Permissions;

use O2Development\Permissions\Commands\PermissionCreate;
use O2Development\Permissions\Commands\PermissionDelete;
use O2Development\Permissions\Commands\PermissionInstall;
use O2Development\Permissions\Commands\RoleCreate;
use O2Development\Permissions\Commands\RoleDelete;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * @inheritdoc
     */
    public function boot()
    {
        $this->handleConfig();
        $this->handleMigrations();

                 if ($this->app->runningInConsole()) {
                     $this->commands([
                         PermissionInstall::class,
                         PermissionCreate::class,
                         PermissionDelete::class,
                         RoleCreate::class,
                         RoleDelete::class
                     ]);
                 }
    }

    /**
     * @inheritdoc
     */
    public function register()
    {
        //
    }

    /**
     * @inheritdoc
     */
    public function provides()
    {
        return [];
    }

    /**
     * Publish and merge the config file.
     *
     * @return void
     */
    private function handleConfig()
    {
        $configPath = __DIR__ . '/../config/config.php';

        $this->publishes([$configPath => config_path('permissions.php')]);

        $this->mergeConfigFrom($configPath, 'permissions');
    }

    /**
     * Publish migrations.
     *
     * @return void
     */
    private function handleMigrations()
    {
        $migrations = [
            'CreatePermissionsTable' => 'create_permissions_table',
            'CreateRolesTable' => 'create_roles_table'
        ];

        foreach ($migrations as $class => $file) {
            if (!class_exists($class)) {
                $timestamp = date('Y_m_d_His', time());

                $this->publishes([
                    __DIR__ . '/../database/migrations/' . $file . '.php.stub' =>
                        database_path('migrations/' . $timestamp . '_' . $file . '.php')
                ], 'migrations');
            }
        }
    }
}