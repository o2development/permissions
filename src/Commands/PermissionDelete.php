<?php
namespace O2Development\Permissions\Commands;

use Illuminate\Console\Command;
use O2Development\Permissions\Contracts\Permission as Contract;
use Spatie\Permission\Contracts\Permission as PermissionContract;

class PermissionDelete extends Command
{
    protected $signature = 'permission:delete 
                {name : The name of the permission} 
                {guard? : The name of the guard}';
    protected $description = 'Delete a permission';
    public function handle()
    {
        $permissionClass = app(Contract::class);

        // Find the permission for defined guard(s)
        // Remove all Users linked.
        // Remove Roles.

        $permission = $permissionClass::create([
            'name' => $this->argument('name'),
            'guard_name' => $this->argument('guard'),
        ]);
        $this->info("Permission `{$permission->name}` created");
    }
}