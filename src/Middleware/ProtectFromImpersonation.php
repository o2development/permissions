<?php

namespace O2Development\Permissions\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use O2Development\Permissions\Services\ImpersonateManager;
class ProtectFromImpersonation
{
    /**
     * Handle an incoming request.
     *
     * @param   \Illuminate\Http\Request  $request
     * @param   \Closure  $next
     * @return  mixed
     */
    public function handle($request, Closure $next)
    {
        $temp = app()->make(ImpersonateManager::class);

        if ($temp->isImpersonating()) {
            return Redirect::back();
        }

        return $next($request);
    }
}