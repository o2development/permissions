<?php
namespace O2Development\Permissions\Commands;

use Illuminate\Console\Command;
use O2Development\Permissions\Contracts\Permission as Contract;
use Spatie\Permission\Contracts\Permission as PermissionContract;

class PermissionInstall extends Command
{
    protected $signature = 'permission:install';
    protected $description = 'Install our permission system';
    public function handle()
    {
        dd(config());

        $reflector = new ReflectionClass('Foo');
        echo $reflector->getFileName();
        exit;
        $permissionClass = app(Contract::class);
        $permission = $permissionClass::create([
            'name' => $this->argument('name'),
            'guard_name' => $this->argument('guard'),
        ]);
        $this->info("Permission `{$permission->name}` created");
    }
}