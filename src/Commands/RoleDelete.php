<?php

namespace O2Development\Permissions\Commands;

use Illuminate\Console\Command;
use O2Development\Permissions\Contracts\Role as RoleContract;
use O2Development\Permissions\Contracts\Permission as PermissionContract;

class RoleDelete extends Command
{
    protected $signature = 'role:delete
        {name : The name of the role}
        {guard? : The name of the guard}';

    protected $description = 'Delete a role';

    public function handle()
    {
        $roleClass = app(RoleContract::class);

        $role = $roleClass::findOrCreate($this->argument('name'), $this->argument('guard'));

        $role->givePermissionTo($this->makePermissions($this->argument('permissions')));

        $this->info("Role `{$role->name}` created");
    }

    protected function makePermissions($string = null)
    {
        if (empty($string)) {
            return;
        }

        $permissionClass = app(PermissionContract::class);

        $permissions = explode('|', $string);

        $models = [];

        foreach ($permissions as $permission) {
            $models[] = $permissionClass::findOrCreate(trim($permission), $this->argument('guard'));
        }

        return collect($models);
    }
}